﻿using Softronix.Infrastructure.BusinessObjects;
using Softronix.ViewModels.Appliances;

namespace Softronix.Services.Interfaces
{
    public interface IAppliancesService
    {
        Task<GenericResult<AppliancesResponseModel>> GetAllAsync();
    }
}
