﻿using Softronix.Infrastructure.BusinessObjects;
using Softronix.ViewModels.Categories;

namespace Softronix.Services.Interfaces
{
    public interface ICategoriesService
    {
        Task<GenericResult<CategoriesResponseModel>> GetAllAsync();
    }
}
