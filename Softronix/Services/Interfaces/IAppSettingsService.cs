﻿using Softronix.Infrastructure.BusinessObjects;
using Softronix.ViewModels.AppSettings;

namespace Softronix.Services.Interfaces
{
    public interface IAppSettingsService
    {
        Task<GenericResult<AppSettingsResponseModel>> GetAllAsync();
    }
}