﻿using Microsoft.EntityFrameworkCore;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Models;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.Appliances;

namespace Softronix.Services.Implementations
{
    public class AppliancesService : IAppliancesService
    {
        private readonly SoftronixContext _dbContext;

        public AppliancesService(SoftronixContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<AppliancesResponseModel>> GetAllAsync()
        {
            try
            {
                var appliances = await (from a in _dbContext.Appliances
                                        join c in _dbContext.Categories on a.CategoryId equals c.Id
                                        select new AppliancesResponseModel
                                        {
                                            DeviceId = a.Id,
                                            Name = a.Name,
                                            Power = a.Power,
                                            Description = a.Description,
                                        }).ToListAsync();

                return GenericResult<AppliancesResponseModel>.Succeed(appliances);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
