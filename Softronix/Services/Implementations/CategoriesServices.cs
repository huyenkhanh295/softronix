﻿using Microsoft.EntityFrameworkCore;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Models;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.Appliances;
using Softronix.ViewModels.Categories;

namespace Softronix.Services.Implementations
{
    public class CategoriesService : ICategoriesService
    {
        private readonly SoftronixContext _dbContext;

        public CategoriesService(SoftronixContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<CategoriesResponseModel>> GetAllAsync()
        {
            try
            {
                var categories = await (from c in _dbContext.Categories.Include(s => s.Appliances)
                                        select new CategoriesResponseModel
                                        {
                                            Id = c.Id,
                                            Category = c.Name,
                                            MinHour = c.MinHour,
                                            MaxHour = c.MaxHour,
                                            Appliances = c.Appliances.Select(s => new AppliancesResponseModel
                                            {
                                                DeviceId = s.Id,
                                                Name = s.Name,
                                                Power = s.Power,
                                            })
                                        }).ToListAsync();

                return GenericResult<CategoriesResponseModel>.Succeed(categories);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
