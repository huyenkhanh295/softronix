﻿using Microsoft.EntityFrameworkCore;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Models;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.AppSettings;

namespace Softronix.Services.Implementations
{
    public class AppSettingsService : IAppSettingsService
    {
        private readonly SoftronixContext _dbContext;
        public AppSettingsService(SoftronixContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<AppSettingsResponseModel>> GetAllAsync()
        {
            try
            {
                var appSettings = await _dbContext.AppSettings.Select(s => new AppSettingsResponseModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Value = s.Value,
                    Description = s.Description,
                }).ToListAsync();
                return GenericResult<AppSettingsResponseModel>.Succeed(appSettings);
            }
            catch (Exception) { throw; }
        }
    }
}
