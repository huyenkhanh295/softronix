﻿using Microsoft.AspNetCore.Mvc;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.Categories;

namespace Softronix.Controllers
{
    [Route("api/categories")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoriesService _categoriesService;

        public CategoriesController(ICategoriesService categoriesService)
        {
            _categoriesService = categoriesService;
        }

        [HttpGet()]
        public async Task<GenericResult<CategoriesResponseModel>> GetAll()
        {
            var response = await _categoriesService.GetAllAsync();
            return response;
        }
    }
}
