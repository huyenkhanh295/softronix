﻿using Microsoft.AspNetCore.Mvc;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.Appliances;

namespace Softronix.Controllers
{
    [Route("api/appliances")]
    [ApiController]
    public class AppliancesController : ControllerBase
    {
        private readonly IAppliancesService _appliancesService;

        public AppliancesController(IAppliancesService appliancesService)
        {
            _appliancesService = appliancesService;
        }

        [HttpGet()]
        public async Task<GenericResult<AppliancesResponseModel>> GetAll()
        {
            var response = await _appliancesService.GetAllAsync();
            return response;
        }
    }
}
