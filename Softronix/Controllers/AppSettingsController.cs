﻿using Microsoft.AspNetCore.Mvc;
using Softronix.Infrastructure.BusinessObjects;
using Softronix.Services.Interfaces;
using Softronix.ViewModels.AppSettings;

namespace Softronix.Controllers
{
    [Route("api/appSettings")]
    [ApiController]
    public class AppSettingsController : ControllerBase
    {
        private readonly IAppSettingsService _appSettingsService;

        public AppSettingsController(IAppSettingsService appSettingsService)
        {
            _appSettingsService = appSettingsService;
        }

        [HttpGet()]
        public async Task<GenericResult<AppSettingsResponseModel>> GetAll()
        {
            var response = await _appSettingsService.GetAllAsync();
            return response;
        }
    }
}
