﻿namespace Softronix.Models
{
    public partial class UserAppliance
    {
        public int UserId { get; set; }
        public int ApplianceId { get; set; }

        public virtual Appliance Appliance { get; set; } = null!;
        public virtual User User { get; set; } = null!;
    }
}
