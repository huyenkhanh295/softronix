﻿using System;
using System.Collections.Generic;

namespace Softronix.Models
{
    public partial class Category
    {
        public Category()
        {
            Appliances = new HashSet<Appliance>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int MinHour { get; set; }
        public int MaxHour { get; set; }
        public string? Description { get; set; }
        public int? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual ICollection<Appliance> Appliances { get; set; }
    }
}
