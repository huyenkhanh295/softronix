﻿using System;
using System.Collections.Generic;

namespace Softronix.Models
{
    public partial class Appliance
    {
        public Appliance()
        {
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int Power { get; set; }
        public int CategoryId { get; set; }
        public string? Description { get; set; }
        public int? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual Category Category { get; set; } = null!;

        public virtual ICollection<User> Users { get; set; }
    }
}
