﻿namespace Softronix.ViewModels.Appliances
{
    public class AppliancesRequestModel
    {
        public string Name { get; set; }
        public int Power { get; set; }
        public int Category { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; } = DateTime.Now;
        public string ModifiedBy { get; set; }
    }
}
