﻿namespace Softronix.ViewModels.Appliances
{

    public class AppliancesResponseModel : BaseModel
    {
        public int DeviceId { get; set; }
        public int Power { get; set; }
    }
}
