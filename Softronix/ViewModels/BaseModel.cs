﻿namespace Softronix.ViewModels
{
    public class BaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
