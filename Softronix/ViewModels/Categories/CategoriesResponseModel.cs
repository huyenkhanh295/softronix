﻿using Softronix.ViewModels.Appliances;

namespace Softronix.ViewModels.Categories
{
    public class CategoriesResponseModel : BaseModel
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public int MinHour { get; set; }
        public int MaxHour { get; set; }
        public IEnumerable<AppliancesResponseModel> Appliances { get; set; }
    }
}
