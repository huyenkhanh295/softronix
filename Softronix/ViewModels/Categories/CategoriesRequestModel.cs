﻿namespace Softronix.ViewModels.Categories
{
    public class CategoriesRequestModel
    {
        public string Name { get; set; }
        public int MinHour { get; set; }
        public int MaxHour { get; set; }
        public string Description { get; set; }
    }
}
