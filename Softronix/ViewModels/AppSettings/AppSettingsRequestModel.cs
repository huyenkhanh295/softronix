﻿namespace Softronix.ViewModels.AppSettings
{
    public class AppSettingsRequestModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
    }
}
