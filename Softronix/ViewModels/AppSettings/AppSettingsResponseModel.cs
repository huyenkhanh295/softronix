﻿namespace Softronix.ViewModels.AppSettings
{
    public class AppSettingsResponseModel : BaseModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
